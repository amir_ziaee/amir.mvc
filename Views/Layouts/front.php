<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .header,.footer{
            width: 100%;
            height: 200px;
        }
        .footer{
            text-align: center;
            font-size: 20px;
            background: #cbff97;
        }
        .header{
            text-align: center;
            font-size: 20px;
            background: #ff9cff;
        }
    </style>
</head>
<body>
<div class="header">Header</div>
<?= $renderdView; ?>
<div class="footer">Foorer</div>
</body>
</html>