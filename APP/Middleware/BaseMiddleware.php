<?php
namespace APP\Middleware;
use APP\Core\Request;

abstract class BaseMiddleware{
    public abstract function handle(Request $request);

}