<?php
namespace APP\Utility;
class config{
    public static function load($configFile)
    {
        return include_once CONFIG_PATH . $configFile . ".php";
    }
}