<?php
namespace APP\Core;

use APP\Utility\Input;

class Request{
        public $uri;
        public  $ip;
        public $agent;
        public $method;
        public $referer;
        public  $params;


public function __construct()
    {
        $this->uri = $_SERVER['REQUEST_URI'];
        $this->ip = $_SERVER['REMOTE_ADDR'];
        $this->agent = $_SERVER['HTTP_USER_AGENT'];
        $this->referer = $_SERVER['HTTP_REFERER'] ?? null;
        $this->method = $_SERVER['REQUEST_METHOD'];
        if(SANITIZE_ALL){
            $this->sanitizeAll();
        }else{
            $this->params = (object)$_REQUEST;
        }

    }

    public function  __get($name)
{
    if ($this->paramExist($name))
        return $this->param($name);
}



public function param($key){
    if ($this->paramExist($key))
        return $_REQUEST[$key];
        return null;
    }

public function paramExist($key){
    if(array_key_exists($key,$_REQUEST))
        return $_REQUEST[$key];
    return false;
}

private function sanitizeAll(){
    $params = new \stdClass;
    foreach ($_REQUEST as $key=>$value){
        $params->$key = Input::clean($value);
    }
    $this->params = $params;
}

}

/*$r = new Request();

var_dump($r);*/