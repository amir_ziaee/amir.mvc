<?php
namespace APP\Services\DBconnection;
    class DBConnection
    {
        private $conn;
        private  static $instance = null;
        public function init($configArray)
        {

            $config = (object)$configArray;


            $this-> conn = new \mysqli($config->host,$config->user,$config->password,$config->name);
        }

        public function hasError(){
            if($this-> conn ->errno){
                return $this-> conn ->error;
            }
            else{
                return false;

            }
        }    public function hasConnError(){
            if($this-> conn ->connect_errno){
                return $this-> conn ->connect_errno;
            }
            else{
                return false;

            }
        }



           public function query(String $sql): \stdClass {
                if($result = $this-> conn ->query($sql)){
                     if(is_bool($result)){
                         return (object)$result;
                     }
              $records=$result->fetch_all(MYSQLI_ASSOC);
                    foreach ($records as $key=>$val){
                        $records[$key]=(object)$val;
                    }
                    return (object)$records;
                }
                return new \stdClass();

            }

        public static function getInstance(){
            if(is_null(self::$instance)){
                $instance = self::$instance = new  DBConnection();
            }
            else{
                $instance = self::$instance;
            }
            return $instance;

        }
        
    }

/*    $db = DBConnection::getInstance();

    $users = $db->query("select * from users");*/

