<?php
namespace APP\Services\Router;
use APP\Core\Request;
use APP\Services\Views\View;

class  router {
    public static $routes;
    const  baseController ="\APP\Controller\\";
    const  baseMiddleware ="\APP\Middleware\\";

    public static function start()
    {
        // get all routes
        self::$routes = include BASE_PATH . "Routes/web.php";
        // get current route
        $currentRoute = self::getCurrentRoute();
        // if current route exist
        if(self::routeExist($currentRoute)){
            //is allowed method
            if(!in_array(strtolower($_SERVER['REQUEST_METHOD']),self::currentMethod($currentRoute))){
                header('HTTP/1.0 403 Forbidden');
//                echo 'You are forbidden!';
                View::load("Error.403");
                die();
            }
            // get current target
            $target = self::currentTarget($currentRoute);
            list($controllerClass,$method) = explode('@',$target);
//            var_dump($controllerClass,$method);
            $fullControllerClass = self::baseController . $controllerClass;
            if(!class_exists($fullControllerClass)){
                echo 'class dose not exist';
                die();
            }
            $controller = new $fullControllerClass;
            if(method_exists($controller,$method)){
                // call method from controller
                $request = new Request();
                //middle ware...
               $middlewareClass = self::baseMiddleware. self::getRouteMiddleWre($currentRoute)."Middleware";

               if (class_exists($middlewareClass)){
//                   var_dump($middlewareClass);
                    $middlewareInstance = new $middlewareClass;
                    $middlewareInstance->handle($request);
               }else{
                   echo 'middleware not exist';
                   die();
               }

                $controller->$method($request);
            }else{
                echo "no";
                die();
            }

            }
        else{
            header("HTTP/1.0 404 Not Found");
//            echo "404 not found";
            View::load("Error.404");
        }


    }
    public static function getCurrentRoute()
    {
        return strtok(strtolower($_SERVER['REQUEST_URI']),'?');
    }
    public static function routeExist($route)
    {
        if(array_key_exists($route,self::$routes)){
            return true;
        }
            return false;

    }

    public static function currentTarget($route)
    {
        return self::$routes[$route]['target'];

    }
    public static function getRouteMiddleWre($route)
    {
        return self::$routes[$route]['middleware'] ?? null;
    }
    public static function currentMethod($route)
    {
        return explode('|',self::$routes[$route]['method']);
    }

}