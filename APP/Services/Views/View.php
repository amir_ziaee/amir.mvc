<?php
namespace APP\Services\Views;

class View{
    public static function is_valid($view){
        $view=str_replace('.','/',$view);
        $fullViewPath=VIEW_PATH.$view.".php";
        if(!file_exists($fullViewPath) or !is_readable($fullViewPath)) {
            return false;
        }
        return $fullViewPath;

    }
    public static function load($view,array $data=array(),$layout=null)
    {
        $fullViewPath =self::is_valid($view);
        if(!$fullViewPath){
            echo 'view not valid !';
            die();
        }
        extract($data);
        if(is_null($layout)){
            include $fullViewPath;
        }else{
            $fullViewLayout=VIEW_PATH."Layouts/".$layout.".php";
            $renderdView=self::render($view,$data);
            include_once $fullViewLayout;
        }


    }public static function render($view,array $data=array())
    {
        $fullViewPath =self::is_valid($view);
        if(!$fullViewPath){
            echo 'view not valid !';
            die();
        }
        extract($data);
        ob_start();
        include_once $fullViewPath;
        return ob_get_clean();

    }

}