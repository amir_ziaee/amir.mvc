<?php
namespace APP\Model;
use \Illuminate\Database\Eloquent\Model;
class BaseModel extends Model {
    public $timestamps = false;

}