<?php
//routes
return[
   '/'=> [
            'method'=>'get',
            'target'=> 'HomeController@index',
             'middleware'=> 'IEblocker'
    ] ,
    '/posts'=> [
            'method'=>'get|post',
            'target'=> 'PostController@post'
    ] ,
    '/login'=> [
            'method'=>'get|post',
            'target'=> 'loginController@login',
             'middleware'=> 'Sanitize'
    ]



];
